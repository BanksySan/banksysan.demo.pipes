﻿namespace Banksysan.Demo.Pipes
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.lblNamedPipesInput = new System.Windows.Forms.Label();
            this.lblMessageRecieved = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.btnNamedPipesSendMessage = new System.Windows.Forms.Button();
            this.txtLogging = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnNamedPipesSendMessage);
            this.groupBox1.Controls.Add(this.lblMessageRecieved);
            this.groupBox1.Controls.Add(this.textBox2);
            this.groupBox1.Controls.Add(this.lblNamedPipesInput);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Location = new System.Drawing.Point(13, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(428, 211);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Named Pipes";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(106, 20);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(316, 63);
            this.textBox1.TabIndex = 0;
            // 
            // lblNamedPipesInput
            // 
            this.lblNamedPipesInput.AutoSize = true;
            this.lblNamedPipesInput.Location = new System.Drawing.Point(7, 20);
            this.lblNamedPipesInput.Name = "lblNamedPipesInput";
            this.lblNamedPipesInput.Size = new System.Drawing.Size(93, 13);
            this.lblNamedPipesInput.TabIndex = 1;
            this.lblNamedPipesInput.Text = "Message to Send:";
            this.lblNamedPipesInput.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblMessageRecieved
            // 
            this.lblMessageRecieved.AutoSize = true;
            this.lblMessageRecieved.Location = new System.Drawing.Point(7, 89);
            this.lblMessageRecieved.Name = "lblMessageRecieved";
            this.lblMessageRecieved.Size = new System.Drawing.Size(102, 13);
            this.lblMessageRecieved.TabIndex = 3;
            this.lblMessageRecieved.Text = "Message Received:";
            this.lblMessageRecieved.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(106, 89);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(316, 63);
            this.textBox2.TabIndex = 2;
            // 
            // btnNamedPipesSendMessage
            // 
            this.btnNamedPipesSendMessage.Location = new System.Drawing.Point(10, 158);
            this.btnNamedPipesSendMessage.Name = "btnNamedPipesSendMessage";
            this.btnNamedPipesSendMessage.Size = new System.Drawing.Size(75, 23);
            this.btnNamedPipesSendMessage.TabIndex = 4;
            this.btnNamedPipesSendMessage.Text = "Send";
            this.btnNamedPipesSendMessage.UseVisualStyleBackColor = true;
            // 
            // txtLogging
            // 
            this.txtLogging.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLogging.Location = new System.Drawing.Point(6, 19);
            this.txtLogging.Multiline = true;
            this.txtLogging.Name = "txtLogging";
            this.txtLogging.ReadOnly = true;
            this.txtLogging.Size = new System.Drawing.Size(237, 177);
            this.txtLogging.TabIndex = 1;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.txtLogging);
            this.groupBox2.Location = new System.Drawing.Point(447, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(249, 212);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Logging";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(710, 238);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "MainForm";
            this.Text = "Pipes Demo";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnNamedPipesSendMessage;
        private System.Windows.Forms.Label lblMessageRecieved;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label lblNamedPipesInput;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox txtLogging;
        private System.Windows.Forms.GroupBox groupBox2;
    }
}

